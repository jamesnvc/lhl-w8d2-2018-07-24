//
//  ViewController.swift
//  MakeThingsPretty
//
//  Created by James Cash on 24-07-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        if traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular {
            print("We have a regular width")
        } else {
            print("Width is compact")
        }

        let btn = UIButton(type: .roundedRect)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Programmatic", for: .normal)
        view.addSubview(btn)
        btn.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        btn.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
    }

    // if we need to respond to the size classes changing in some way that we can't handle by just having different constraints installed for different size classes
    // for example, if we've created constraints programatically & want to disable some & enable others
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        if traitCollection.horizontalSizeClass != previousTraitCollection?.horizontalSizeClass {
            print("Width size class changed")
        } else {
            print("No change")
        }
    }

    // if we need to respond to rotations in some way, but the size classes won't change on rotation (e.g. for an iPad which will always be wR x hR)
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if size.width > size.height {
            print("Rotating to landscape")
        } else {
            print("Rotating to portrait")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

